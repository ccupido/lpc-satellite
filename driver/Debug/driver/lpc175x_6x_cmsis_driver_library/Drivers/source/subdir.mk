################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/debug_frmwrk.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_adc.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_can.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_clkpwr.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_dac.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_emac.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_exti.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpdma.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpio.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2c.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2s.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_iap.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_libcfg_default.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_mcpwm.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_nvic.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pinsel.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pwm.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_qei.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rit.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rtc.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_spi.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_ssp.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_systick.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_timer.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_uart.c \
../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_wdt.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/debug_frmwrk.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_adc.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_can.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_clkpwr.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_dac.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_emac.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_exti.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpdma.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpio.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2c.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2s.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_iap.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_libcfg_default.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_mcpwm.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_nvic.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pinsel.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pwm.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_qei.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rit.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rtc.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_spi.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_ssp.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_systick.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_timer.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_uart.o \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_wdt.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/debug_frmwrk.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_adc.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_can.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_clkpwr.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_dac.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_emac.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_exti.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpdma.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpio.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2c.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2s.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_iap.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_libcfg_default.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_mcpwm.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_nvic.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pinsel.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pwm.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_qei.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rit.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rtc.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_spi.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_ssp.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_systick.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_timer.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_uart.d \
./driver/lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_wdt.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Drivers/source/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Drivers/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


