################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/ADC.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/EMAC.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/Retarget.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/easyweb.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/tcpip.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/ADC.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/EMAC.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/Retarget.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/easyweb.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/tcpip.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/ADC.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/EMAC.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/Retarget.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/easyweb.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/tcpip.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


