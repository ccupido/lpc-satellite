################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/clock-arch.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/emac.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/main.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/clock-arch.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/emac.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/main.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/clock-arch.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/emac.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/main.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/lpc17xx_port/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


