################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/psock.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/timer.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-fw.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-neighbor.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-split.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip_arp.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uiplib.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/psock.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/timer.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-fw.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-neighbor.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-split.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip_arp.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uiplib.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/psock.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/timer.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-fw.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-neighbor.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-split.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip_arp.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uiplib.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


