################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/demo.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/hiduser.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbcore.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbdesc.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbhw.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbuser.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/demo.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/hiduser.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbcore.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbdesc.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbhw.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbuser.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/demo.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/hiduser.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbcore.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbdesc.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbhw.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbuser.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


