################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/DiskImg.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/memory.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/mscuser.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbcore.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbdesc.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbhw.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbuser.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/DiskImg.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/memory.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/mscuser.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbcore.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbdesc.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbhw.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbuser.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/DiskImg.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/memory.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/mscuser.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbcore.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbdesc.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbhw.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbuser.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


