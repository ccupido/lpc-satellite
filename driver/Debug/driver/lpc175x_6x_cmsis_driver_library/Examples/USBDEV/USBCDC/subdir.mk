################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/cdcuser.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/serial.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbcore.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbdesc.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbhw.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbuser.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/vcomdemo.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/cdcuser.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/serial.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbcore.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbdesc.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbhw.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbuser.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/vcomdemo.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/cdcuser.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/serial.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbcore.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbdesc.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbhw.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbuser.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/vcomdemo.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


