################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/adcuser.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbcore.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdesc.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdmain.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbhw.c \
../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbuser.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/adcuser.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbcore.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdesc.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdmain.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbhw.o \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbuser.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/adcuser.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbcore.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdesc.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdmain.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbhw.d \
./driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbuser.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


