################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/lpc175x_6x_cmsis_driver_library/Examples/PWR/RTC_DeepPWD/rtc_deeppwd.c 

OBJS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/PWR/RTC_DeepPWD/rtc_deeppwd.o 

C_DEPS += \
./driver/lpc175x_6x_cmsis_driver_library/Examples/PWR/RTC_DeepPWD/rtc_deeppwd.d 


# Each subdirectory must supply rules for building sources it contributes
driver/lpc175x_6x_cmsis_driver_library/Examples/PWR/RTC_DeepPWD/%.o: ../driver/lpc175x_6x_cmsis_driver_library/Examples/PWR/RTC_DeepPWD/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


