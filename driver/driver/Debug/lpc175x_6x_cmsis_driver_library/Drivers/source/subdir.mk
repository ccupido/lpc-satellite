################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Drivers/source/debug_frmwrk.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_adc.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_can.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_clkpwr.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_dac.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_emac.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_exti.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpdma.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpio.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2c.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2s.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_iap.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_libcfg_default.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_mcpwm.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_nvic.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pinsel.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pwm.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_qei.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rit.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rtc.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_spi.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_ssp.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_systick.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_timer.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_uart.c \
../lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_wdt.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Drivers/source/debug_frmwrk.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_adc.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_can.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_clkpwr.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_dac.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_emac.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_exti.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpdma.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpio.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2c.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2s.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_iap.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_libcfg_default.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_mcpwm.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_nvic.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pinsel.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pwm.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_qei.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rit.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rtc.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_spi.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_ssp.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_systick.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_timer.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_uart.o \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_wdt.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Drivers/source/debug_frmwrk.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_adc.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_can.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_clkpwr.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_dac.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_emac.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_exti.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpdma.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_gpio.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2c.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_i2s.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_iap.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_libcfg_default.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_mcpwm.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_nvic.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pinsel.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_pwm.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_qei.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rit.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_rtc.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_spi.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_ssp.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_systick.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_timer.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_uart.d \
./lpc175x_6x_cmsis_driver_library/Drivers/source/lpc17xx_wdt.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Drivers/source/%.o: ../lpc175x_6x_cmsis_driver_library/Drivers/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


