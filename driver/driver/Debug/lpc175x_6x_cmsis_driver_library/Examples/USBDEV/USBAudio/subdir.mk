################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/adcuser.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbcore.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdesc.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdmain.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbhw.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbuser.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/adcuser.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbcore.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdesc.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdmain.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbhw.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbuser.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/adcuser.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbcore.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdesc.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbdmain.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbhw.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/usbuser.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/%.o: ../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBAudio/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


