################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/DiskImg.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/memory.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/mscuser.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbcore.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbdesc.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbhw.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbuser.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/DiskImg.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/memory.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/mscuser.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbcore.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbdesc.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbhw.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbuser.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/DiskImg.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/memory.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/mscuser.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbcore.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbdesc.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbhw.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/usbuser.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/%.o: ../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBMassStorage/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


