################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/cdcuser.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/serial.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbcore.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbdesc.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbhw.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbuser.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/vcomdemo.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/cdcuser.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/serial.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbcore.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbdesc.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbhw.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbuser.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/vcomdemo.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/cdcuser.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/serial.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbcore.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbdesc.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbhw.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/usbuser.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/vcomdemo.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/%.o: ../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBCDC/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


