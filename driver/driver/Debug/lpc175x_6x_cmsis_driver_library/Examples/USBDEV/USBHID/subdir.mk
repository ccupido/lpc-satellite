################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/demo.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/hiduser.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbcore.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbdesc.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbhw.c \
../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbuser.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/demo.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/hiduser.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbcore.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbdesc.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbhw.o \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbuser.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/demo.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/hiduser.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbcore.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbdesc.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbhw.d \
./lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/usbuser.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/%.o: ../lpc175x_6x_cmsis_driver_library/Examples/USBDEV/USBHID/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


