################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/ADC.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/EMAC.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/Retarget.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/easyweb.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/tcpip.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/ADC.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/EMAC.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/Retarget.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/easyweb.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/tcpip.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/ADC.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/EMAC.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/Retarget.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/easyweb.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/tcpip.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/%.o: ../lpc175x_6x_cmsis_driver_library/Examples/EMAC/Easy_Web/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


