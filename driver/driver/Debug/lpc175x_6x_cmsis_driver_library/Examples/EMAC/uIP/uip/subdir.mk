################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/psock.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/timer.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-fw.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-neighbor.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-split.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip_arp.c \
../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uiplib.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/psock.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/timer.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-fw.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-neighbor.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-split.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip_arp.o \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uiplib.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/psock.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/timer.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-fw.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-neighbor.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip-split.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uip_arp.d \
./lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/uiplib.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/%.o: ../lpc175x_6x_cmsis_driver_library/Examples/EMAC/uIP/uip/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


