################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/NXP_logo.c \
../lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/Terminal_9_12x6.c \
../lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/drv_glcd.c \
../lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/glcd_ll.c \
../lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/lcdtest.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/NXP_logo.o \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/Terminal_9_12x6.o \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/drv_glcd.o \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/glcd_ll.o \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/lcdtest.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/NXP_logo.d \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/Terminal_9_12x6.d \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/drv_glcd.d \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/glcd_ll.d \
./lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/lcdtest.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/%.o: ../lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


