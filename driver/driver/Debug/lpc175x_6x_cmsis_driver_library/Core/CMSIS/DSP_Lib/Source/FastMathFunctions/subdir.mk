################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_f32.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q15.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q31.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_f32.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q15.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q31.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q15.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q31.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_f32.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q15.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q31.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_f32.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q15.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q31.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q15.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q31.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_f32.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q15.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q31.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_f32.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q15.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q31.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q15.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q31.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/%.o: ../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Source/FastMathFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


