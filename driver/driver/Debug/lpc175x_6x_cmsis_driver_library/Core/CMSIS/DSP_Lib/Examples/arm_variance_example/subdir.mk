################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/arm_variance_example_f32.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM0.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM3.c \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM4.c 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/arm_variance_example_f32.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM0.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM3.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM4.o 

C_DEPS += \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/arm_variance_example_f32.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM0.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM3.d \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/system_ARMCM4.d 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/%.o: ../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_variance_example/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\CMSIS\Include" -I"D:\AlbertaSatCode\lpctest\Drivers\lpc175x_6x_cmsis_driver_library\Core\Device\NXP\LPC17xx\Include" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


