################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_signal_converge_example/ARM/startup_ARMCM0.s \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_signal_converge_example/ARM/startup_ARMCM3.s \
../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_signal_converge_example/ARM/startup_ARMCM4.s 

OBJS += \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_signal_converge_example/ARM/startup_ARMCM0.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_signal_converge_example/ARM/startup_ARMCM3.o \
./lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_signal_converge_example/ARM/startup_ARMCM4.o 


# Each subdirectory must supply rules for building sources it contributes
lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_signal_converge_example/ARM/%.o: ../lpc175x_6x_cmsis_driver_library/Core/CMSIS/DSP_Lib/Examples/arm_signal_converge_example/ARM/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU Assembler'
	arm-none-eabi-gcc -c -x assembler-with-cpp -D__REDLIB__ -DDEBUG -D__CODE_RED -g3 -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


