/*************************************************************************
 *    File name   : NXP_logo.h
 *    Description : NXP Logo include file
 **************************************************************************/
#include "../../../../../driver/lpc175x_6x_cmsis_driver_library/Examples/LCD/NOKIA6610_LCD/drv_glcd.h"


#ifndef __NXP_LOGO_H
#define __NXP_LOGO_H

extern Bmp_t NXP_Logo;

#endif // __NXP_LOGO_H
