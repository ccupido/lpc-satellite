# README #

For use in the LPCXpresso eclipse IDE. 
FreeRTOS contains all RTOS code. 
Driver contains LPC board drivers compliant with the CMSIS library originally with the example code. 

CSP contains all CSP code and is the build target to make. All other projects included are linked via static library into the CSP project.