/*
    FreeRTOS V8.0.1 - Copyright (C) 2014 Real Time Engineers Ltd.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    ***NOTE*** The exception to the GPL is included to allow you to distribute
    a combined work that includes FreeRTOS without being obliged to provide the
    source code for proprietary components outside of the FreeRTOS kernel.
    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public 
    License and the FreeRTOS license exception along with FreeRTOS; if not it 
    can be viewed here: http://www.freertos.org/a00114.html and also obtained 
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"

/* Demo includes. */
#include "basic_io.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_libcfg_default.h"

/* CSP includes. */
#include <csp/csp.h>
#include <csp/interfaces/csp_if_kiss.h>
#include <csp/drivers/usart.h>
#include <csp/arch/csp_thread.h>


/* The task function. */
void vTaskFunction( void *pvParameters );

/* Define the strings that will be passed in as the task parameters.  These are
defined const and off the stack to ensure they remain valid when the tasks are
executing. */
const char *pcTextForTask1 = "Task 1 is running\n";
const char *pcTextForTask2 = "Task 2 is running\n";

uint8_t helloworld[] = "Hello World! \n\r";

/* CSP defines. */
#define MY_ADDRESS 2
#define DEST_ADDRESS 1
#define POWER_HK_PORT 8
#define POWER_REQ_PORT 9
#define BUFFER_SIZE 300

usart_callback_t usart_callback = NULL;

int havepacket = 0;



/* NanoPower housekeeping struct */
typedef struct __attribute__((packed)) {
uint16_t vboost[3]; //! Voltage of boost converters [mV] [PV1, PV2, PV3]
uint16_t vbatt; //! Voltage of battery [mV]
uint16_t curin[3]; //! Current in [mA]
uint16_t cursun; //! Current from boost converters [mA]
uint16_t cursys; //! Current out of battery [mA]
uint16_t reserved1; //! Reserved for future use
uint16_t curout[6]; //! Current out (switchable outputs) [mA]
uint8_t output[8]; //! Status of outputs**
uint16_t output_on_delta[8]; //! Time till power on** [s]
uint16_t output_off_delta[8]; //! Time till power off** [s]
uint16_t latchup[6]; //! Number of latch-ups
uint32_t wdt_i2c_time_left; //! Time left on I2C wdt [s]
uint32_t wdt_gnd_time_left; //! Time left on I2C wdt [s]
uint8_t wdt_csp_pings_left[2]; //! Pings left on CSP wdt
uint32_t counter_wdt_i2c; //! Number of WDT I2C reboots
uint32_t counter_wdt_gnd; //! Number of WDT GND reboots
uint32_t counter_wdt_csp[2]; //! Number of WDT CSP reboots
uint32_t counter_boot; //! Number of EPS reboots
int16_t temp[6]; //! Temperatures [degC] [0 = TEMP1, TEMP2, TEMP3, TEMP4, BP4a, BP4b]*
uint8_t bootcause; //! Cause of last EPS reset
uint8_t battmode; //! Mode for battery [0 = initial, 1 = undervoltage, 2 = nominal, 3 = batteryfull]
uint8_t pptmode; //! Mode of PPT tracker [1=MPPT, 2=FIXED]
uint16_t reserved2;
} eps_hk_t;

CSP_DEFINE_TASK(task_client)
{
	for( ;; )
	{
		uint8_t data_out[] = {1,2,3,4,5,6};
		uint8_t *dataout;
		dataout = data_out;
		uint8_t data_in[200] = {0};
		uint8_t *datain;
		datain = data_in;
		int status = csp_transaction(2, DEST_ADDRESS, POWER_REQ_PORT, 300, dataout, 6, datain, -1);
		vPrintString(status);
		vTaskDelay( 1000 / portTICK_RATE_MS );
	}
}


/*-----------------------------------------------------------*/

int main( void )
{
	UART_CFG_Type UARTConfigStruct;
	UART_FIFO_CFG_Type UARTFIFOConfigStruct;
	PINSEL_CFG_Type PinCfg;

	/*
	 * Initialize UART3 pin connect
	 */
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Pinnum = 0;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 1;
	PINSEL_ConfigPin(&PinCfg);

	UART_ConfigStructInit(&UARTConfigStruct);
	UART_Init(LPC_UART3, &UARTConfigStruct);
	UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);
	UART_FIFOConfig((LPC_UART_TypeDef *)LPC_UART3, &UARTFIFOConfigStruct);
	UART_TxCmd(LPC_UART3, ENABLE);

	/* CSP initialization */

	csp_buffer_init(10 , BUFFER_SIZE);
	csp_init(MY_ADDRESS);

	static csp_iface_t csp_if_kiss;
	static csp_kiss_handle_t csp_kiss_driver;
	csp_kiss_init(&csp_if_kiss, &csp_kiss_driver, usart_putc, usart_insert, "KISS");


	void my_usart_rx(uint8_t * buf, int len, void * pxTaskWoken) {
		csp_kiss_rx(&csp_if_kiss, buf, len, pxTaskWoken);
	}
	usart_set_callback(my_usart_rx);


	csp_route_set(DEST_ADDRESS, &csp_if_kiss, CSP_NODE_MAC);
	csp_route_start_task(600, 2);


	csp_thread_handle_t handle_client;
	csp_thread_create(task_client, (signed char *) "CLIENT", 1000, NULL, 3, &handle_client);


	//xTaskCreate( vTaskFunction, "Task 1", 240, (void*)pcTextForTask1, 1, NULL );

	vTaskStartScheduler();	
	
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTaskFunction( void *pvParameters )
{
char *pcTaskName;

	/* The string to print out is passed in via the parameter.  Cast this to a
	character pointer. */
	pcTaskName = ( char * ) pvParameters;
	char nom = 0;

	/* As per most tasks, this task is implemented in an infinite loop. */
	for( ;; )
	{
		/* Print out the name of this task. */
		vPrintString( pcTaskName );

		//UART_Send(LPC_UART3, helloworld, sizeof(helloworld), NONE_BLOCKING);


		/* Delay for a period.  This time we use a call to vTaskDelay() which
		puts the task into the Blocked state until the delay period has expired.
		The delay period is specified in 'ticks'. */
		vTaskDelay( 1000 / portTICK_RATE_MS );
	}
}

/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* This function will only be called if an API call to create a task, queue
	or semaphore fails because there is too little heap RAM remaining. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	/* This function will only be called if a task overflows its stack.  Note
	that stack overflow checking does slow down the context switch
	implementation. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* This example does not use the idle hook to perform any processing. */
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
	/* This example does not use the tick hook to perform any processing. */
}

void usart_insert(char c, void *pxTaskWoken){
	//vPrintString(c);
}

void usart_putc(char c){
	UART_SendByte(LPC_UART3, c);
}

void usart_set_callback(usart_callback_t callback) {
	usart_callback = callback;
}


