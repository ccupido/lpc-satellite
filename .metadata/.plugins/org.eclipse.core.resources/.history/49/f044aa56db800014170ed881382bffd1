/*
    FreeRTOS V8.0.1 - Copyright (C) 2014 Real Time Engineers Ltd.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    ***NOTE*** The exception to the GPL is included to allow you to distribute
    a combined work that includes FreeRTOS without being obliged to provide the
    source code for proprietary components outside of the FreeRTOS kernel.
    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public 
    License and the FreeRTOS license exception along with FreeRTOS; if not it 
    can be viewed here: http://www.freertos.org/a00114.html and also obtained 
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "consoleprint.h"

/* Demo includes. */
#include "basic_io.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_libcfg_default.h"

/* CSP includes. */
#include <csp/csp.h>
#include <csp/interfaces/csp_if_kiss.h>
#include <csp/drivers/usart.h>
#include <csp/arch/csp_thread.h>

/* FreeRTOS tasks */
void vCSPHandler( void *pvParameters );
void vUART_Go( void *pvParameters);


/* Other functions */
void my_usart_rx(uint8_t * buf, int len, void * pxTaskWoken);
void UART_IntErr(uint8_t bLSErrType);
void UART3_IRQHandler(void);
void UART_IntReceive(void);

void UART_Test(void);

/* CSP defines. */
#define MY_ADDRESS 2
#define DEST_ADDRESS 1
#define POWER_HK_PORT 8
#define POWER_REQ_PORT 9
#define BUFFER_SIZE 300

usart_callback_t usart_callback = NULL;
static csp_iface_t csp_if_kiss;
static csp_kiss_handle_t csp_kiss_driver;

int setup = 0;
int irqfired = 0;
volatile int UART_Done = 0;
volatile uint8_t uart_dump[300];
volatile int uart_length;

UART_CFG_Type UARTConfigStruct;
UART_FIFO_CFG_Type UARTFIFOConfigStruct;
PINSEL_CFG_Type PinCfg;


/* NanoPower housekeeping struct */
typedef struct __attribute__((packed)) {
uint16_t vboost[3]; //! Voltage of boost converters [mV] [PV1, PV2, PV3]
uint16_t vbatt; //! Voltage of battery [mV]
uint16_t curin[3]; //! Current in [mA]
uint16_t cursun; //! Current from boost converters [mA]
uint16_t cursys; //! Current out of battery [mA]
uint16_t reserved1; //! Reserved for future use
uint16_t curout[6]; //! Current out (switchable outputs) [mA]
uint8_t output[8]; //! Status of outputs**
uint16_t output_on_delta[8]; //! Time till power on** [s]
uint16_t output_off_delta[8]; //! Time till power off** [s]
uint16_t latchup[6]; //! Number of latch-ups
uint32_t wdt_i2c_time_left; //! Time left on I2C wdt [s]
uint32_t wdt_gnd_time_left; //! Time left on I2C wdt [s]
uint8_t wdt_csp_pings_left[2]; //! Pings left on CSP wdt
uint32_t counter_wdt_i2c; //! Number of WDT I2C reboots
uint32_t counter_wdt_gnd; //! Number of WDT GND reboots
uint32_t counter_wdt_csp[2]; //! Number of WDT CSP reboots
uint32_t counter_boot; //! Number of EPS reboots
int16_t temp[6]; //! Temperatures [degC] [0 = TEMP1, TEMP2, TEMP3, TEMP4, BP4a, BP4b]*
uint8_t bootcause; //! Cause of last EPS reset
uint8_t battmode; //! Mode for battery [0 = initial, 1 = undervoltage, 2 = nominal, 3 = batteryfull]
uint8_t pptmode; //! Mode of PPT tracker [1=MPPT, 2=FIXED]
uint16_t reserved2;
} eps_hk_t;

/*
CSP_DEFINE_TASK(task_client)
{
	for( ;; )
	{
		vPrintString("Client Start \n");

		csp_socket_t *socketout = csp_socket(CSP_SO_CRC32REQ);
		csp_socket_t *socketin = csp_socket(CSP_SO_CRC32REQ);
		csp_conn_t *connin;
		csp_conn_t *connout;
		csp_packet_t * query = csp_buffer_get(BUFFER_SIZE);
		query->data[0] = 5;
		query->length = 1;

		if(setup == 0){
			csp_bind(socketin, POWER_HK_PORT);
			csp_bind(socketout, POWER_REQ_PORT);
			csp_listen(socketin, 5);
			setup = 1;
			vPrintString("Setup Successful \n");
		}

		csp_packet_t *hk_data;
		connout = csp_connect(1, DEST_ADDRESS, POWER_REQ_PORT, 1000, CSP_O_CRC32);
		csp_send(connout, query, 1000);

		if((connin = csp_accept(socketin, 500)) != NULL){
			hk_data = csp_read(connin, 100);
			csp_close(connin);
			csp_close(connout);
			vPrintString("Have Packet! \n");
			if((hk_data->data[7]) == 0xEF){
				vPrintString("Data Verified! \n");
			}
		}
		else{
			csp_close(connout);
			vPrintString("No Packet This Time \n");
		}
	}
}
*/


/*-----------------------------------------------------------*/

void main( void )
{
	/*
	 * Initialize UART3 pin connect
	 */
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Pinnum = 0;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 1;
	PINSEL_ConfigPin(&PinCfg);

	/* Initialize FIFO, UART3, and RX Interrupts */
	UART_ConfigStructInit(&UARTConfigStruct);
	UART_Init(LPC_UART3, &UARTConfigStruct);
	UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);
	UART_FIFOConfig((LPC_UART_TypeDef *)LPC_UART3, &UARTFIFOConfigStruct);
	UART_TxCmd(LPC_UART3, ENABLE);
	UART_IntConfig((LPC_UART_TypeDef *)LPC_UART3, UART_INTCFG_RBR, ENABLE);
	UART_IntConfig((LPC_UART_TypeDef *)LPC_UART3, UART_INTCFG_RLS, ENABLE);
	NVIC_SetPriority(UART3_IRQn, 0);
	NVIC_EnableIRQ(UART3_IRQn);


	csp_buffer_init(10 , BUFFER_SIZE);
	csp_init(MY_ADDRESS);

	csp_kiss_init(&csp_if_kiss, &csp_kiss_driver, usart_putc, usart_insert, "KISS");
	usart_set_callback(my_usart_rx);
	csp_route_set(DEST_ADDRESS, &csp_if_kiss, CSP_NODE_MAC);
	csp_route_start_task(200, 2);

	//csp_thread_handle_t handle_client;
	//csp_thread_create(task_client, (signed char *) "CLIENT", 400, NULL, 3, &handle_client);


	xTaskCreate( vCSPHandler, "CSP", 500, NULL, 3, NULL );
	//xTaskCreate( vUART_Go, "RX", 300, NULL, 4, NULL );

	vTaskStartScheduler();

	/*
	while(1){
		for(int i=0;i<1000000;i++){
			if(i == 500000){
				consoleprint("Sent \n");
				UART_SendByte(LPC_UART3, 0x43);
			}
		}
	}
	*/
	
}
/*-----------------------------------------------------------*/

void vCSPHandler( void *pvParameters )
{
	for( ;; ){
		vPrintString("Client Start \n");

		uint8_t data_buf[300];
		int data_length;

		csp_socket_t *socketout = csp_socket(CSP_SO_CRC32REQ);
		csp_socket_t *socketin = csp_socket(CSP_SO_CRC32REQ);
		csp_conn_t *connin;
		csp_conn_t *connout;
		csp_packet_t * query = csp_buffer_get(BUFFER_SIZE);
		query->data[0] = 5;
		query->length = 1;

		if(setup == 0){
			csp_bind(socketin, POWER_HK_PORT);
			csp_bind(socketout, POWER_REQ_PORT);
			csp_listen(socketin, 5);
			setup = 1;
			vPrintString("Setup Successful \n");
		}

		csp_packet_t *hk_data;
		connout = csp_connect(1, DEST_ADDRESS, POWER_REQ_PORT, 1000, CSP_O_CRC32);
		csp_send(connout, query, 1000);
		taskENTER_CRITICAL();

		/* Wait for UART Rx interrupt to go and complete */
		while(UART_Done == 0){}
		UART_Done = 0;
		data_length = uart_length;
		for(int i = 0;i<data_length;i++){
				data_buf[i] = (uint8_t) uart_dump[i];
			}

		//my_usart_rx(data_buf, data_length, NULL);
		taskEXIT_CRITICAL();
		vPrintString(data_buf);
/*
		if((connin = csp_accept(socketin, 500)) != NULL){
			hk_data = csp_read(connin, 100);
			csp_close(connin);
			csp_close(connout);
			vPrintString("Have Packet! \n");
			if((hk_data->data[0]) == 0xEF){
				vPrintString("Data Verified! \n");
			}
		}
		else{
			csp_close(connout);
			vPrintString("No Packet This Time \n");
			if(irqfired == 1){
				vPrintString("IRQ fired but no cake \n");
			}
		}
		*/
	}
}

/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* This function will only be called if an API call to create a task, queue
	or semaphore fails because there is too little heap RAM remaining. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	/* This function will only be called if a task overflows its stack.  Note
	that stack overflow checking does slow down the context switch
	implementation. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* This example does not use the idle hook to perform any processing. */
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
	/* This example does not use the tick hook to perform any processing. */
}

void usart_insert(char c, void *pxTaskWoken){
	//vPrintString(c);
}

void usart_putc(char c){
	UART_SendByte(LPC_UART3, c);
}

void usart_set_callback(usart_callback_t callback) {
	usart_callback = callback;
}

void my_usart_rx(uint8_t * buf, int len, void * pxTaskWoken) {
	csp_kiss_rx(&csp_if_kiss, buf, len, pxTaskWoken);
}


/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*********************************************************************//**
 * @brief		UART3 interrupt handler sub-routine
 * @param[in]	None
 * @return 		None
 **********************************************************************/
void UART3_IRQHandler(void)
{
	uint32_t intsrc, tmp, tmp1;

	/* Determine the interrupt source */
	intsrc = UART_GetIntId(LPC_UART3);
	tmp = intsrc & UART_IIR_INTID_MASK;
	irqfired = 1;

	// Receive Line Status
	if (tmp == UART_IIR_INTID_RLS){
		// Check line status
		tmp1 = UART_GetLineStatus(LPC_UART3);
		// Mask out the Receive Ready and Transmit Holding empty status
		tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE \
				| UART_LSR_BI | UART_LSR_RXFE);
		// If any error exist
		if (tmp1) {
				UART_IntErr(tmp1);
		}
	}

	// Receive Data Available or Character time-out
	if ((tmp == UART_IIR_INTID_RDA) || (tmp == UART_IIR_INTID_CTI)){
			UART_IntReceive();
			//UART_Test();
	}
	UART_Done = 1;
}

/********************************************************************//**
 * @brief 		UART receive function (ring buffer used)
 * @param[in]	None
 * @return 		None
 *********************************************************************/
void UART_IntReceive(void)
{

	uint8_t tmpc[300];
	uint8_t *pntr = tmpc;
	uint32_t rLen;
	rLen = UART_Receive((LPC_UART_TypeDef *)LPC_UART3, pntr, 300, NONE_BLOCKING);
	for(int i = 0;i<rLen;i++){
		uart_dump[i] = tmpc[i];
	}
	uart_length = rLen;
	//my_usart_rx(tmpc, rLen, 0);
}
/*********************************************************************//**
 * @brief		UART Line Status Error
 * @param[in]	bLSErrType	UART Line Status Error Type
 * @return		None
 **********************************************************************/
void UART_IntErr(uint8_t bLSErrType)
{
	while(1){
		if(bLSErrType == UART_LSR_OE){
			vPrintString("UART RX Overrun \n");
		}
	}
}

void UART_Test(void){
	uint8_t tmpc[300];
	uint8_t *pntr = tmpc;
	uint32_t rLen;
	rLen = UART_Receive((LPC_UART_TypeDef *)LPC_UART3, pntr, 300, NONE_BLOCKING);
		UART_Done = 1;
	}
}


void vUART_Go( void *pvParameters){
	while(1){
		vPrintString("Sent \n");
		UART_SendByte(LPC_UART3, 0x42);
		while(UART_Done == 0){}
		if(UART_Done == 1){
			UART_Done = 0;
			vPrintString("RX Done \n");
		}
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}


