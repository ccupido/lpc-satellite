################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../csp/src/crypto/csp_hmac.c \
../csp/src/crypto/csp_sha1.c \
../csp/src/crypto/csp_xtea.c 

OBJS += \
./csp/src/crypto/csp_hmac.o \
./csp/src/crypto/csp_sha1.o \
./csp/src/crypto/csp_xtea.o 

C_DEPS += \
./csp/src/crypto/csp_hmac.d \
./csp/src/crypto/csp_sha1.d \
./csp/src/crypto/csp_xtea.d 


# Each subdirectory must supply rules for building sources it contributes
csp/src/crypto/%.o: ../csp/src/crypto/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -DDEBUG -D__USE_CMSIS=CMSISv1p30_LPC17xx -D__CODE_RED -D__REDLIB__ -I"D:\LPCXpresso\lpcxpresso\tools\lib\gcc\arm-none-eabi\4.8.4\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\LPCXpresso\lpcxpresso\tools\arm-none-eabi\include" -I"D:\AlbertaSatCode\lpctest\CSPTest\csp\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\demo_code" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\portable" -O3 -g3 -fsigned-char -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


