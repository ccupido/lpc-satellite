################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../csp/src/arch/macosx/csp_malloc.c \
../csp/src/arch/macosx/csp_queue.c \
../csp/src/arch/macosx/csp_semaphore.c \
../csp/src/arch/macosx/csp_system.c \
../csp/src/arch/macosx/csp_thread.c \
../csp/src/arch/macosx/csp_time.c \
../csp/src/arch/macosx/pthread_queue.c 

OBJS += \
./csp/src/arch/macosx/csp_malloc.o \
./csp/src/arch/macosx/csp_queue.o \
./csp/src/arch/macosx/csp_semaphore.o \
./csp/src/arch/macosx/csp_system.o \
./csp/src/arch/macosx/csp_thread.o \
./csp/src/arch/macosx/csp_time.o \
./csp/src/arch/macosx/pthread_queue.o 

C_DEPS += \
./csp/src/arch/macosx/csp_malloc.d \
./csp/src/arch/macosx/csp_queue.d \
./csp/src/arch/macosx/csp_semaphore.d \
./csp/src/arch/macosx/csp_system.d \
./csp/src/arch/macosx/csp_thread.d \
./csp/src/arch/macosx/csp_time.d \
./csp/src/arch/macosx/pthread_queue.d 


# Each subdirectory must supply rules for building sources it contributes
csp/src/arch/macosx/%.o: ../csp/src/arch/macosx/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -DDEBUG -D__USE_CMSIS=CMSISv1p30_LPC17xx -D__CODE_RED -D__REDLIB__ -I"D:\LPCXpresso\lpcxpresso\tools\lib\gcc\arm-none-eabi\4.8.4\include" -I"D:\LPCXpresso\lpcxpresso\tools\arm-none-eabi\include" -I"D:\AlbertaSatCode\lpctest\CSPTest\csp\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\demo_code" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\portable" -O0 -g3 -fsigned-char -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


