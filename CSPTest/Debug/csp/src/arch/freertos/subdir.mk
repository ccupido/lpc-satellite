################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../csp/src/arch/freertos/csp_malloc.c \
../csp/src/arch/freertos/csp_queue.c \
../csp/src/arch/freertos/csp_semaphore.c \
../csp/src/arch/freertos/csp_system.c \
../csp/src/arch/freertos/csp_thread.c \
../csp/src/arch/freertos/csp_time.c 

OBJS += \
./csp/src/arch/freertos/csp_malloc.o \
./csp/src/arch/freertos/csp_queue.o \
./csp/src/arch/freertos/csp_semaphore.o \
./csp/src/arch/freertos/csp_system.o \
./csp/src/arch/freertos/csp_thread.o \
./csp/src/arch/freertos/csp_time.o 

C_DEPS += \
./csp/src/arch/freertos/csp_malloc.d \
./csp/src/arch/freertos/csp_queue.d \
./csp/src/arch/freertos/csp_semaphore.d \
./csp/src/arch/freertos/csp_system.d \
./csp/src/arch/freertos/csp_thread.d \
./csp/src/arch/freertos/csp_time.d 


# Each subdirectory must supply rules for building sources it contributes
csp/src/arch/freertos/%.o: ../csp/src/arch/freertos/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -DDEBUG -D__USE_CMSIS=CMSISv1p30_LPC17xx -D__CODE_RED -D__REDLIB__ -I"D:\LPCXpresso\lpcxpresso\tools\lib\gcc\arm-none-eabi\4.8.4\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\LPCXpresso\lpcxpresso\tools\arm-none-eabi\include" -I"D:\AlbertaSatCode\lpctest\CSPTest\csp\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\demo_code" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\portable" -O3 -g3 -fsigned-char -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


