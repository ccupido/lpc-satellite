################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../csp/src/csp_buffer.c \
../csp/src/csp_conn.c \
../csp/src/csp_crc32.c \
../csp/src/csp_debug.c \
../csp/src/csp_endian.c \
../csp/src/csp_io.c \
../csp/src/csp_port.c \
../csp/src/csp_route.c \
../csp/src/csp_service_handler.c \
../csp/src/csp_services.c \
../csp/src/csp_sfp.c 

OBJS += \
./csp/src/csp_buffer.o \
./csp/src/csp_conn.o \
./csp/src/csp_crc32.o \
./csp/src/csp_debug.o \
./csp/src/csp_endian.o \
./csp/src/csp_io.o \
./csp/src/csp_port.o \
./csp/src/csp_route.o \
./csp/src/csp_service_handler.o \
./csp/src/csp_services.o \
./csp/src/csp_sfp.o 

C_DEPS += \
./csp/src/csp_buffer.d \
./csp/src/csp_conn.d \
./csp/src/csp_crc32.d \
./csp/src/csp_debug.d \
./csp/src/csp_endian.d \
./csp/src/csp_io.d \
./csp/src/csp_port.d \
./csp/src/csp_route.d \
./csp/src/csp_service_handler.d \
./csp/src/csp_services.d \
./csp/src/csp_sfp.d 


# Each subdirectory must supply rules for building sources it contributes
csp/src/%.o: ../csp/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -DDEBUG -D__USE_CMSIS=CMSISv1p30_LPC17xx -D__CODE_RED -D__REDLIB__ -I"D:\LPCXpresso\lpcxpresso\tools\lib\gcc\arm-none-eabi\4.8.4\include" -I"D:\AlbertaSatCode\lpctest\driver\driver\lpc175x_6x_cmsis_driver_library\Drivers\include" -I"D:\LPCXpresso\lpcxpresso\tools\arm-none-eabi\include" -I"D:\AlbertaSatCode\lpctest\CSPTest\csp\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\demo_code" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\portable" -O3 -g3 -fsigned-char -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


