################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../csp/examples/kiss.c \
../csp/examples/simple.c 

OBJS += \
./csp/examples/kiss.o \
./csp/examples/simple.o 

C_DEPS += \
./csp/examples/kiss.d \
./csp/examples/simple.d 


# Each subdirectory must supply rules for building sources it contributes
csp/examples/%.o: ../csp/examples/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -DDEBUG -D__USE_CMSIS=CMSISv1p30_LPC17xx -D__CODE_RED -D__REDLIB__ -I"D:\LPCXpresso\lpcxpresso\tools\lib\gcc\arm-none-eabi\4.8.4\include" -I"D:\LPCXpresso\lpcxpresso\tools\arm-none-eabi\include" -I"D:\AlbertaSatCode\lpctest\CSPTest\csp\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\demo_code" -I"D:\AlbertaSatCode\lpctest\CMSISv1p30_LPC17xx\inc" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\include" -I"D:\AlbertaSatCode\lpctest\FreeRTOS_Library\portable" -O0 -g3 -fsigned-char -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


